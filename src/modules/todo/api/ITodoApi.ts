import { CrudApi } from '@/shared/api/crud_api/CrudApi';
import { TodoRequest, TodoResponse } from './TodoTypes';

export type ITodoApi = CrudApi<TodoResponse, TodoResponse, TodoRequest>;
