import { AxiosInstance } from 'axios';
import { CrudApiImpl } from '@/shared/api/crud_api/CrudApiImpl';
import { ITodoApi } from '@/modules/todo/api/ITodoApi';
import { Todo } from '../Todo';

export class TodoApi extends CrudApiImpl<Todo> implements ITodoApi {
  constructor(protected axios: AxiosInstance) {
    super(axios, '/todos');
  }
}
