export type Todo = {
    id: number;
    userId: number;
    title: String;
    completed: boolean;
}