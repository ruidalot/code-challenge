import '@/styles/index.scss';
import Vue from 'vue';
import Router from 'vue-router';
import Toasted from 'vue-toasted';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { router } from '@/routing/router';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import store from './store';
import App from './App.vue';

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)

Vue.use(Router);
Vue.use(Toasted, {
  theme: 'toasted-primary',
  position: 'top-right',
  duration: '3000',
});

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = process.env.VUE_APP_API_URL;

Vue.router = router;

Vue.config.productionTip = false;
const root = new Vue({
  router,
  store,
  render: (h) => h(App),
});

root.$mount('#app');
