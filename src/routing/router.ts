import Router from 'vue-router';
import { Routes } from '@/routing/Routes';
import Todos from '@/modules/Todos/components/Todos.vue'
import Statistics from '@/modules/Statistics/components/Statistics.vue'

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: Routes.USERS,
      component: () => import('@/modules/user/screens/UserListing.vue'),
    }, 
    {
      path: '/todos',
      name: Routes.TODOS,
      component:  () => import('@/modules/todo/screens/TodoListing.vue'),
    },
    {
      path: '/statistics',
      name: 'STATISTICS',
      component: Statistics,
    } 
  ],
});
