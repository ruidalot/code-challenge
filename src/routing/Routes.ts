export enum Routes {
  USERS = 'USERS',
  TODOS = 'TODOS',
  STATISTICS = 'STATISTICS'
}
